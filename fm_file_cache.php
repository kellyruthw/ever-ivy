<?php
class FileCache {

	private static $instance;

	private $cache_folder;
	private $cache_time_sec = 900;
	private $cache_ext = ".html";
	private $ignore_keys = array();

	public function get($key) {
		$cache_file = $this->getCacheFilename($key);
		if (!$ignore && file_exists($cache_file) && time() - $this->cache_time_sec < filemtime($cache_file)) { //check Cache exist and it's not expired.
			return file_get_contents($cache_file);
		}
		return null;
	}

	public function deleteOldCachedFiles() {
		$files = glob($this->cache_folder."*");
		$now   = time();
		foreach ($files as $file) {
		  if (is_file($file)) {
			if ($now - filemtime($file) >= $this->cache_time_sec) {
			  unlink($file);
			}
		  }
		}
	}

	public function set($key, $value) {
		$cache_file = $this->getCacheFilename($key);
		$ignore = (in_array($key,$this->ignore_keys)) ?true : false; //check if url is in ignore list
		if(!$ignore){
			if (!is_dir($this->cache_folder)) { //create a new folder if we need to
				mkdir($this->cache_folder);
			}
			$fp = fopen($cache_file, 'w');  //open file for writing
			fwrite($fp, $value); //write contents of the output buffer in Cache file
			fclose($fp); //Close file pointer
		}
	}

	public static function getInstance()
    {
        if (!isset(self::$instance)) {
			self::$instance = new FileCache('module_cache/');
		}
        return self::$instance;
	}

	private function getCacheFilename($key) {
		return $this->cache_folder.md5($key).$this->cache_ext;
	}

	private function __construct($cache_folder) {
		$this->cache_folder = $cache_folder;
	}
}
?>
