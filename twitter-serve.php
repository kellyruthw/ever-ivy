<?php
	
	require_once('./fm_file_cache.php');


	function get_twitter_embed_code( $image_id ,$size ) {
		
		$curl = curl_init();
		
		// if(wp_is_mobile() ){
		// 	$size  = 350;
		// }
        curl_setopt_array(
            $curl,
            array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL            => 'https://publish.twitter.com/oembed?maxwidth='.$size.'&url=https://twitter.com/Interior/status/' . $image_id,
                CURLOPT_USERAGENT      => 'Simple Twitter Embed Wordpress Plugin'
            )
        );
        $result = json_decode( curl_exec( $curl ) );
        $http_status = curl_getinfo( $curl, CURLINFO_HTTP_CODE );
        curl_close( $curl );
        if ( $http_status === 200 ) {
			return $result;
        }
        return '';
    }


	ob_start('ob_gzhandler');

	header("Content-type:application/json");

	

	
	$id = $_GET['id'];
	$width = $_GET['width'];
	$ret = get_twitter_embed_code($id , $width);
	echo str_replace('\n', '', json_encode($ret,JSON_UNESCAPED_SLASHES));

	$size = ob_get_length();
	header("Cache-Control: public, max-age=900");
	header("Content-Length: $size");

	$time_end = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
	//$ga->reportTiming('applicaster_api', '/json.php', round($time_end * 1000));

	ob_end_flush(); // All output buffers must be flushed here
	flush();

?>