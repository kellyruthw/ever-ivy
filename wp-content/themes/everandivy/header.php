<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bestsubscriptions
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> class="js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="https://use.typekit.net/mrl4ani.css" />

	<!-- NEEDS GTM -->


	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
	<!-- NEEDS GOOGLE TAG MANAGER -->

<div id="page" class="site main-wrap">
	<header id="masthead" class="site-header">
		<nav id="site-navigation" class="main-navigation">
			<button class="hamburger hamburger--squeeze" type="button" class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
			  <span class="hamburger-box">
			    <span class="hamburger-inner"></span>
			  </span>
			</button>
			<div class="top-nav">
				<div class="container">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="https://everandivy.com/wp-content/uploads/2019/07/logo-everandivy.jpg" class="logo"></a>
				</div>
			</div>
			<div class="main-nav">
				<div class="container">
					<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					) );
					?>
				</div>
			</div>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
