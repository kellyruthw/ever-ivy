<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bestsubscriptions
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="content">
				<?php $term = get_queried_object(); ?>
				
				
				<div class="container">
					<h1><?php echo $term->name; ?></h1>
					<div class="cats">
						<?php
						wp_nav_menu( array(
							'theme_location' => 'menu-5',
							'menu_id'        => 'recipe-categories',
						) );
						?>
					</div>
					<div class="articles recipes">
						
						<?php
							$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
							$args = array(
							    'post_type' => 'recipe',
							    'recipe_cat' => $term->slug,
							    'posts_per_page' => 12,
								'paged' => $paged,
							);
							$query = new WP_Query( $args );
						?>
						<?php
							if ($query->have_posts()) {
						        while ( $query->have_posts() ) : $query->the_post(); ?>
						 
						        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								    <a href="<?php echo esc_url( get_permalink() ); ?>">
									    <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-play.svg" class="play-btn">
									    <?php the_post_thumbnail('recipe-thumb'); ?>
									</a>
								    <div class="copy">
								        <h2><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a></h2>
								    </div>
							    </article>
						         
						        <?php endwhile;
						         
						         
						} // end of check for query having posts
						     
						// use reset postdata to restore orginal query
						wp_reset_postdata();
						 
						?>
						
					      <div class="navigation">						
							<div class="next">
								<?php next_posts_link( 'Older Entries', $the_query2->max_num_pages ); ?>
							</div>
						</div>
						<?php 
						// clean up after the query and pagination
						wp_reset_postdata(); 
						?>
						
						<div class="ad">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/ad.jpg">
						</div>
					</div>
					<?php
					if ( wp_is_mobile() ) { ?>
						<div class="view-more mobile">
							<div class="page-load-status">
							  <div class="loader-ellips infinite-scroll-request">
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							  </div>
							</div>
							<button class="btn">Show me more</button>
						</div>
						
						<?php get_sidebar(); ?>

					<?php } else { ?>
						<?php get_sidebar(); ?>
						<div class="view-more">
							<div class="page-load-status">
							  <div class="loader-ellips infinite-scroll-request">
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							  </div>
							</div>
							<button class="btn">Show me more</button>
						</div>
					<?php }
				?>
					</div>
				</div>
					
			</div> <!-- content -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>

				