<?php

add_filter('img_caption_shortcode', 'wpse81532_caption', 10, 3);

function wpse81532_caption($na, $atts, $content)
{
    if(Utils::getInstance()->getPageType() != "infinite"){
        return $content;

    }
    extract(shortcode_atts(array(
        'id'    => '',
        'class_to_add' => 'lazy featured-image',
        'width' => '',
        'src' => '',
        'caption' => ''
    ), $atts));
    

    
    // preg_match('/src="(.*?)?"/', $content, $matches);
    // $srcUrl = $matches[1];
    // $src = 'src=';
    // $src_pos = stripos($content, $src);
    $res = str_replace(' src=', ' data-src=' , $content);

    
  

    $class = 'class=';
    $cls_pos = stripos($res, $class);
    if ($cls_pos === false) {
        $res = str_replace('<img', '<img class=""' , $res);
    } else {
        $res = substr_replace($res, esc_attr($class_to_add) . ' ', $cls_pos + strlen($class) + 1, 0);
    }

    $res = '<figure class="featured-image-wrapper">'.$res."</figure><figcaption>".$caption.'</figcaption>';
    return $res;
    


}