<?php

	
	
	function fm_instagram_func( $atts ) {
		if ( is_feed( CUSTOM_INSTANT_ARTICLES_SLUG ) || is_feed( CUSTOM_INSTANT_ARTICLES_SLUG_DEV )) {
			return fm_instagram_instant_article_post($atts);
		} else {
			return fm_instagram_regular_post($atts);
		}
	}
	function fm_instagram_regular_post($atts) {
		$atts = shortcode_atts( array(
			'url' => '',
			'hidecaption' => false,
			), $atts, 'fm_instagram' );
		preg_match("/instagram.com\/p\/(.*?)\//",$atts['url'],$m);
		$post_id = $m[1];
		//$embedHtml = get_embed_code($post_id, 600, $atts['hidecaption']);
		$html = '<div id="'.$post_id.'" class="bu-social-embed bu-social-embed-instagram" data-embed-type="instagram" data-embed-id="'.$post_id.'">'
				.'</div>';
		
		
		return $html;
	}
	function fm_instagram_instant_article_post($atts) {
		$atts = shortcode_atts( array(
			'url' => '',
			'hidecaption' => false,
			), $atts, 'fm_instagram' );
		preg_match("/instagram.com\/p\/(.*?)\//",$atts['url'],$m);
		$post_id = $m[1];
		$embedHtml = get_embed_code($post_id, 600, $atts['hidecaption']);
		$html = $embedHtml;
		return $html;
	}

	add_shortcode( 'fm_instagram', 'fm_instagram_func' );
	add_shortcode( 'instagram', 'fm_instagram_func' );


	 /**
     * Get embed code from Instagram
     *
     * @param   string   $image_id      Image ID on Instagram
     * @param   integer  $size          Size (width) in pixels
     * @param   boolean  $hide_caption  Should caption be hidden
     *
     * @return  html                    Returns embed HTML code
     */
    function get_embed_code( $image_id = null, $size = 600, $hide_caption = false ) {
		
		$curl = curl_init();
		if(wp_is_mobile() ){
			$size  = 350;
		}
        curl_setopt_array(
            $curl,
            array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL            => 'https://api.instagram.com/oembed/?url=https://instagr.am/p/' . $image_id . '&maxwidth=' . $size . '&hidecaption=' . $hide_caption,
                CURLOPT_USERAGENT      => 'Simple Instagram Embed Wordpress Plugin'
            )
        );
        $result = json_decode( curl_exec( $curl ) );
        $http_status = curl_getinfo( $curl, CURLINFO_HTTP_CODE );
        curl_close( $curl );
        if ( $http_status === 200 ) {
            return $result->html;
        }
        return '';
    }

	function get_twitter_embed_code( $image_id = null  , $size) {
		
		$curl = curl_init();
		
        curl_setopt_array(
            $curl,
            array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL            => 'https://publish.twitter.com/oembed?maxwidth='.$size.'&url=https://twitter.com/Interior/status/' . $image_id,
                CURLOPT_USERAGENT      => 'Simple Twitter Embed Wordpress Plugin'
            )
        );
        $result = json_decode( curl_exec( $curl ) );
        $http_status = curl_getinfo( $curl, CURLINFO_HTTP_CODE );
        curl_close( $curl );
        if ( $http_status === 200 ) {
			return $result->html;
        }
        return '';
    }

	function fm_pinterest_func( $atts ) {
		if ( is_feed( CUSTOM_INSTANT_ARTICLES_SLUG ) || is_feed( CUSTOM_INSTANT_ARTICLES_SLUG_DEV )) {
			return fm_pinterest_instant_article_post($atts);
		} else {
			return fm_pinterest_regular_post($atts);
		}
	}
	function fm_pinterest_regular_post( $atts ) {
		$atts = shortcode_atts( array(
			'url' => '',
			'hidecaption' => true,
			), $atts, 'fm_pinterest' );
		//print_r($atts);
		preg_match("/www.pinterest.com\/pin\/(.*?)\//",$atts['url'],$m);
		$post_id = $m[1];
		$html = '<div class="bu-social-embed bu-social-embed-pinterest" data-embed-type="pinterest" data-embed-id="'.$post_id.'">'
					.'<a data-pin-do="embedPin" '.($atts['hidecaption']==true ? '' : 'data-pin-terse="true"').' href="'.$atts['url'].'"></a>'
				.'</div>';
		return $html;
	}
	function fm_pinterest_instant_article_post( $atts ) {
		$atts = shortcode_atts( array(
			'url' => '',
			'hidecaption' => true,
			), $atts, 'fm_pinterest' );
		//print_r($atts);
		preg_match("/www.pinterest.com\/pin\/(.*?)\//",$atts['url'],$m);
		$post_id = $m[1];
		$html = '<div class="embed"><a data-pin-do="embedPin" '.($atts['hidecaption']==true ? '' : 'data-pin-terse="true"').' href="'.$atts['url'].'"></a><script async defer src="//assets.pinterest.com/js/pinit.js"></script></div>';
		return $html;
	}


	add_shortcode( 'fm_pinterest', 'fm_pinterest_func' );

	function fm_giphy_func( $atts ) {
		if ( is_feed( CUSTOM_INSTANT_ARTICLES_SLUG ) || is_feed( CUSTOM_INSTANT_ARTICLES_SLUG_DEV )) {
			return fm_giphy_instant_article_post($atts);
		} else {
			return fm_giphy_regular_post($atts);
		}
	}
	function fm_giphy_regular_post( $atts ) {
		$atts = shortcode_atts( array(
			'url' => '',
			), $atts, 'fm_giphy' );
		preg_match("/media.giphy.com\/media\/(.*?)\//",$atts['url'],$m);
		$post_id = explode('/', $m[1]);
		$html = '<div class="bu-social-embed bu-social-embed-giphy" data-embed-type="giphy" data-embed-id="'.$post_id[0].'">'
					.'<iframe style="margin: 0 auto; display: block !important; max-width: 100%;" src="https://giphy.com/embed/'.$post_id[0].'" width="360" height="480" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>'
				.'</div>';
		return $html;
	}
	function fm_giphy_instant_article_post( $atts ) {
		$atts = shortcode_atts( array(
			'url' => '',
			), $atts, 'fm_giphy' );
		preg_match("/media.giphy.com\/media\/(.*?)\//",$atts['url'],$m);
		$post_id = explode('/', $m[1]);
		$html = '<iframe style="margin: 0 auto; display: block !important; max-width: 100%;" src="https://giphy.com/embed/'.$post_id[0].'" width="360" height="480" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>';
		return $html;
	}
	add_shortcode( 'fm_giphy', 'fm_giphy_func' );

	function fm_facebook_func( $atts ) {
		if ( is_feed( CUSTOM_INSTANT_ARTICLES_SLUG ) || is_feed( CUSTOM_INSTANT_ARTICLES_SLUG_DEV )) {
			return fm_facebook_instant_article_post($atts);
		} else {
			return fm_facebook_regular_post($atts);
		}
	}
	function fm_facebook_regular_post( $atts ) {
		$atts = shortcode_atts( array(
			'url' => '',
			), $atts, 'fm_facebook' );
		preg_match("/\/(posts|videos)\/(.*)/", $atts['url'], $m);
		$post_type = $m[1];
		$post_id = $m[2];
		$html = '<div class="bu-social-embed bu-social-embed-facebook" data-embed-type="facebook" data-embed-id="'.$post_id.'">';
		if ($post_type == 'posts') {
			$html.= '<div class="fb-post" data-href="'.$atts['url'].'"></div>';
		} else {
			$html.= '<div class="fb-video" data-href="'.$atts['url'].'" data-allowfullscreen="true" data-width="500"></div>';
		}
		$html.=	'</div>';
		return $html;
	}
	function fm_facebook_instant_article_post( $atts ) {
		$atts = shortcode_atts( array(
			'url' => '',
			), $atts, 'fm_facebook' );
		preg_match("/\/(posts|videos)\/(.*)/", $atts['url'], $m);
		$post_type = $m[1];
		$post_id = $m[2];
		$html = '<div class="embed">';
		if ($post_type == 'posts') {
			$html.= '<div class="fb-post" data-href="'.$atts['url'].'" data-width="500"></div>';
		} else {
			$html.= '<div class="fb-video" data-href="'.$atts['url'].'" data-allowfullscreen="true" data-width="500"></div>';
		}
		$html.='<script>(function(d, s, id) {'
		  .'var js, fjs = d.getElementsByTagName(s)[0];'
		  .'if (d.getElementById(id)) return;'
		  .'js = d.createElement(s); js.id = id;'
		  .'.js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&amp;version=v2.5";'
		  .'fjs.parentNode.insertBefore(js, fjs);'
		.'}(document, \'script\', \'facebook-jssdk\'));</script>';
		$html.='</div>';
		return $html;
	}
	add_shortcode( 'fm_facebook', 'fm_facebook_func' );

	function fm_twitter_func( $atts ) {
		if ( is_feed( CUSTOM_INSTANT_ARTICLES_SLUG ) || is_feed( CUSTOM_INSTANT_ARTICLES_SLUG_DEV )) {
			return fm_twitter_instant_article_post($atts);
		} else {
			return fm_twitter_regular_post($atts);
		}
	}
	function fm_twitter_regular_post( $atts ) {
		$atts = shortcode_atts( array(
			'url' => '',
			), $atts, 'fm_twitter' );
			preg_match("/status\/(.*)/",$atts['url'],$m);
		$post_id = $m[1];
		//$html = '<div class="bu-social-embed bu-social-embed-twitter" data-embed-type="twitter" data-embed-id="'.$post_id.'">';
		//$html.= get_twitter_embed_code($post_id);
		//$html.=	'</div>';



		$html = '<div id="'.$post_id.'" class="bu-social-embed bu-social-embed-twitter" data-embed-type="twitter" data-embed-id="'.$post_id.'">'
				.'</div>';
		
		
		return $html;
	}
	function fm_twitter_instant_article_post( $atts ) {
		$atts = shortcode_atts( array(
			'url' => '',
			), $atts, 'fm_twitter' );
			preg_match("/status\/(.*)/",$atts['url'],$m);
		$post_id = $m[1];
		$html = '<div class="embed">';
		$html .= get_twitter_embed_code($post_id);
		$html .='</div>';
		return $html;
	}
	add_shortcode( 'fm_twitter', 'fm_twitter_func' );

	function fm_youtube_func( $atts ) {
		if ( is_feed( CUSTOM_INSTANT_ARTICLES_SLUG ) || is_feed( CUSTOM_INSTANT_ARTICLES_SLUG_DEV )) {
			return fm_youtube_instant_article_post($atts);
		} else {
			return fm_youtube_regular_post($atts);
		}
	}

	function fm_youtube_regular_post( $atts ) {
		$atts = shortcode_atts( array(
			'url' => '',
			), $atts, 'fm_youtube' );
			preg_match("/\?v=(.*)/",$atts['url'],$m);
		$post_id = explode('&', $m[1]);
		$html = '<div class="bu-social-embed bu-social-embed-youtube" data-embed-type="youtube" data-embed-id="'.$post_id[0].'">';
			$html.=	'<iframe style="max-width: 100%;" width="560" height="315" src="https://www.youtube.com/embed/'.$post_id[0].'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
		$html.=	'</div>';
		return $html;
	}

	function fm_youtube_instant_article_post( $atts ) {
		$atts = shortcode_atts( array(
			'url' => '',
			), $atts, 'fm_youtube' );
			preg_match("/\?v=(.*)/",$atts['url'],$m);
		$post_id = explode('&', $m[1]);
		$html =	'<iframe style="max-width: 100%;" width="560" height="315" src="https://www.youtube.com/embed/'.$post_id[0].'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
		return $html;
	}

	add_shortcode( 'fm_youtube', 'fm_youtube_func' );

	function fm_imgur_func( $atts ) {
		$atts = shortcode_atts( array(
			'url' => '',
			'hidecaption' => false,
			), $atts, 'fm_imgur' );
			preg_match("/gallery\/(.*)/",$atts['url'],$m);
		$post_id = $m[1];
		//print_r($atts['hidecaption']);
		$html = '<div class="bu-social-embed bu-social-embed-imgur" data-embed-type="imgur" data-embed-id="'.$post_id.'">';
		$html.= '<blockquote class="imgur-embed-pub" lang="en" data-id="'.$post_id.'" '.($atts['hidecaption']==true ? 'data-context="false"' : '').'><a href="//imgur.com/'.$post_id.'"></a></blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script>';
		$html.=	'</div>';
		return $html;
	}
	add_shortcode( 'fm_imgur', 'fm_imgur_func' );
?>
