<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bestsubscriptions
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="content">
				<h1><?php the_archive_title(); ?></h1>
				
				<?php $this_category = get_category($cat); ?>
				<?php if ($this_category->category_parent == 0) { ?>
				<?php   // show a slider for each category, with clickable thumbanils for each subcategory
 
				$categories = get_categories( array(
				    'orderby' => 'name',
				    'parent'  => $cat
				) );
				
				if ($categories)
				{
					foreach ( $categories as $category ){
						
					?>
					<div class="headline">
						<?php
							$term = $category->term_id;
							$img = get_field('cat_image', 'term_'.$term);
							$category_link = get_category_link( $term );
						?>
								
						<a href="<?php echo esc_url( $category_link ); ?>">
							<img src="<?php echo $img; ?>" alt="<?php echo $img; ?>" /><h2><?php echo $category->name; ?></h2>
							
						</a>
						<a href="<?php echo esc_url( $category_link ); ?>" class="link">View All in <span class="coral"><?php echo $category->name; ?></span></a>
					</div>
						
					<div class="triple-hero">
				    	
				    <?php
					    $args = array(
						    'posts_per_page' => 3,
						    'cat' => $category->cat_ID
						);
						
					    $subcat_posts = get_posts($args);					    
					    $counter = 0;
						
						foreach($subcat_posts as $subcat_post) {
							
								$postID = $subcat_post->ID;
								$url = wp_get_attachment_url( get_post_thumbnail_id($postID) );
								
								if ($counter == 1) :
						            echo "<div class='small-hero'>";
						        endif;
								?>
								
								<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								     <a href="<?php echo esc_url( get_permalink() ); ?>">
									    <img src="<?php echo $url ?>">
									</a>
												    <div class="copy">
								        <a href="<?php echo get_permalink($postID); ?>"><h2><?php echo get_the_title($postID); ?></h2></a>
								        <a href="<?php echo get_permalink($postID); ?>" class="link">Keep Reading <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-spoon.svg"></a>
								    </div>
							    </article>
							    <?php $counter++; ?>
				    
				    
					    <?php } ?>
						</div> <!-- small hero -->
				    </div> <!-- triple hero -->
						
				    <?php
					} 
				}
				} else{ ?>
				
				<div class="container">
					<div class="articles">
					
							<?php
							// set the "paged" parameter (use 'page' if the query is on a static front page)
							$page = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;
							
							?>
							
							
							<?php
							// the loop
							while ( have_posts() ) : the_post(); 
													
							?>
							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							    <a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_post_thumbnail('homepage-recent-thumb'); ?></a>
							    <div class="copy">
							        <h2><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a></h2>
							        <?php the_excerpt(); ?>
							    </div>
						    </article>
							<?php endwhile; ?>
							<?php next_posts_link( 'Older Entries', $the_query2->max_num_pages ); ?>
							<div class="navigation">						
								<div class="next">
									<?php next_posts_link( 'Older Entries', $the_query2->max_num_pages ); ?>
								</div>
							</div>
							<?php 
							// clean up after the query and pagination
							wp_reset_postdata(); 
							?>
							
							<!-- /2699062/ei_category_d970x250_1 -->
							<div id='div-gpt-ad-1558571820919-0' class="ad">
								<script>
								googletag.cmd.push(function() { googletag.display('div-gpt-ad-1558571820919-0'); });
								</script>
							</div>
							
						</div>
						<?php
							if ( wp_is_mobile() ) { ?>
								<div class="view-more mobile">
									<div class="page-load-status">
									  <div class="loader-ellips infinite-scroll-request">
									    <span class="loader-ellips__dot"></span>
									    <span class="loader-ellips__dot"></span>
									    <span class="loader-ellips__dot"></span>
									    <span class="loader-ellips__dot"></span>
									  </div>
									</div>
									<button class="btn">Show me more</button>
								</div>
								
								<?php get_sidebar(); ?>
	
							<?php } else { ?>
								<?php get_sidebar(); ?>
								<div class="view-more">
									<div class="page-load-status">
									  <div class="loader-ellips infinite-scroll-request">
									    <span class="loader-ellips__dot"></span>
									    <span class="loader-ellips__dot"></span>
									    <span class="loader-ellips__dot"></span>
									    <span class="loader-ellips__dot"></span>
									  </div>
									</div>
									<button class="btn">Show me more</button>
								</div>
							<?php }
						?>
						
					</div>
				</div>
					
				<?php }
			
				
				?>
				
					
			</div> <!-- content -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>

				