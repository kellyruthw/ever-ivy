<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bestsubscriptions
 */

?>

	</div><!-- #content -->

</div><!-- #page -->
<!--<div class="ad"><img src="<?php echo get_template_directory_uri(); ?>/assets/ad.jpg"></div> -->
<footer id="colophon" class="site-footer">
	<a href="/"><img src="https://everandivy.com/wp-content/uploads/2019/07/logo-everandivy.jpg" /></a>
	<div class="copyright">
		<?php wp_nav_menu( array( 'theme_location' => 'menu-4' ) ); ?>
		<p>©2019 So Yummy All Rights Reserved.</p>
	</div>
</footer><!-- #colophon -->


<?php wp_footer(); ?>

</body>
</html>
