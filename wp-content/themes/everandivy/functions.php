<?php
define('__ROOT__', dirname(__FILE__));
// Let users define their own feed slug.
if ( ! defined( 'CUSTOM_INSTANT_ARTICLES_SLUG' ) ) {
	define( 'CUSTOM_INSTANT_ARTICLES_SLUG', 'fbia' );
}
if ( ! defined( 'CUSTOM_INSTANT_ARTICLES_SLUG_DEV' ) ) {
	define( 'CUSTOM_INSTANT_ARTICLES_SLUG_DEV', 'fbia_dev' );
}
include('shortcodes/shortcodes_includes.php');


/**
 * soyummy functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package soyummy
 */

if ( ! function_exists( 'soyummy_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function soyummy_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on soyummy, use a find and replace
		 * to change 'soyummy' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'soyummy', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'homepage-recent-thumb', 360, 202, true );
		add_image_size( 'homepage-large-hero', 730, 410, true );
		add_image_size( 'sidebar-trending', 80, 70, true );
		add_image_size( 'single-thumbnail', 770, 433, true );
		add_image_size( 'recipe-thumb', 243, 243, true );
		add_image_size( 'recipe-more', 257, 257, true );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'soyummy' ),
			'menu-2' => esc_html__( 'Footer', 'soyummy' ),
			'menu-3' => esc_html__( 'Categories', 'soyummy' ),
			'menu-4' => esc_html__( 'Copyright', 'soyummy' ),
			'menu-5' => esc_html__( 'Recipe Categories', 'soyummy' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'soyummy_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
	}
endif;
add_action( 'after_setup_theme', 'soyummy_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function soyummy_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'soyummy_content_width', 640 );
}
add_action( 'after_setup_theme', 'soyummy_content_width', 0 );


/**
 * Enqueue scripts and styles.
 */
function soyummy_scripts() {
	
	wp_enqueue_style( 'soyummy-style', get_stylesheet_uri() );	

  wp_register_script('google_dfp_call','https://securepubads.g.doubleclick.net/tag/js/gpt.js');
	$jtime = filemtime( get_template_directory() . '/assets/js/main.min.js' );
	wp_enqueue_script('main_script',get_template_directory_uri().'/assets/js/main.min.js',[ 'google_dfp_call'],$jtime ,true);


}
add_action( 'wp_enqueue_scripts', 'soyummy_scripts' );

function custom_load_scripts() { 
   // Load if not mobile 
   if (  wp_is_mobile() ) { 
      // Example script 
      wp_enqueue_style( 'soyummy-style-menu', get_template_directory_uri() . '/assets/css/hamburger.min.css');
      } 
}

add_action( 'wp_enqueue_scripts', 'custom_load_scripts' );


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        } elseif ( is_tag() ) {

            $title = single_tag_title( '', false );

        } elseif ( is_author() ) {

            $title = '<span class="vcard">' . get_the_author() . '</span>' ;

        }

    return $title;

});

// Replaces the excerpt "Read More" text by a link
function new_excerpt_more($more) {
       global $post;
	return ' ...</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');


// Reducdes word count in exceprt
function tn_custom_excerpt_length( $length ) {
return 17;
}
add_filter( 'excerpt_length', 'tn_custom_excerpt_length', 999 );

// Add image to menu items
add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects( $items, $args ) {
	
	// loop
	foreach( $items as &$item ) {
		
		// vars
		$icon = get_field('cat_image', $item); ?>
		
		<?php $item->title .= '<img src="'.$icon.'" />'; ?>

		
	<?php }
	
	
	// return
	return $items;
	
}

// Most popular post
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

// adds post count
function wpb_get_post_views($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}

function centil_infinite_scroll($pid){
  if (is_single()) { ?>
    <script type="text/javascript" >
      jQuery(document).ready(function($) {

        $(window).scroll(function() {
          var footerPos = $('footer').last().position().top;
          var pos = $(window).scrollTop();

          // Load next article
          if (pos+(screen.height*4) > footerPos) {
            if ($(".centil-infinite-scroll").first().hasClass('working')) {
              return false;
            } else {
              $(".centil-infinite-scroll").first().addClass('working');
            }

            var centilNextPostId = $(".centil-infinite-scroll").first().text();
            var data = {
              'action': 'centil_is',
              'centilNextPostId': centilNextPostId
            };

            // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
            jQuery.post('<?php echo admin_url("admin-ajax.php"); ?>', data, function(response) {
              $(".centil-infinite-scroll").first().replaceWith(response);
            }, 'html');
          }

          // Update new URL
          var currUrl = $(".centil-post-header").first().attr("url");
          var currTitle = $(".centil-post-header").first().attr("title");

          if ($(".centil-post-header").length > 1 && history.pushState) {
            for (var i=0; i<$(".centil-post-header").length; i++) {
              if (pos+(screen.height/2) >= $(".centil-post-header").eq(i).next().position().top) {
                currUrl = $(".centil-post-header").eq(i).attr("url");
                currTitle = $(".centil-post-header").eq(i).attr("title");
              }
            }
          }
          if (location.href != currUrl) {
            history.pushState({}, currTitle, currUrl);
          }
        });
      });
    </script>

  <?php }
}

add_action( 'wp_foot', 'centil_infinite_scroll' );

function centil_infinite_scroll_callback() {

  if (isset($_POST['centilNextPostId']) && $_POST['centilNextPostId']) {
    // The Query
    $the_query = new WP_Query(array('p'=>$_POST['centilNextPostId']));

    // The Loop
    if ( $the_query->have_posts() ) {
      while ( $the_query->have_posts() ) {
        $the_query->the_post();
        get_template_part( 'template-parts/content', 'single' );
      }
    }
    /* Restore original Post Data */
    wp_reset_postdata();
  }

  wp_die();
}
add_action( 'wp_ajax_centil_is', 'centil_infinite_scroll_callback' );
add_action( 'wp_ajax_nopriv_centil_is', 'centil_infinite_scroll_callback' );



//setup editor menu

add_action("admin_init", "dashboard_init");

function dashboard_init() {
  wp_register_style('custom-css_cms', get_bloginfo('template_url') . '/admin/css/style.css');
  wp_enqueue_style('custom-css_cms');

  wp_enqueue_script('global_js' , get_bloginfo('template_url') . '/admin/js/scripts.js');
}


function my_add_mce_button() {
      if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
          return;
      }

      if ( 'true' == get_user_option( 'rich_editing' ) ) {
          add_filter( 'mce_external_plugins', 'my_add_tinymce_plugin' );
          add_filter( 'mce_buttons', 'my_register_mce_button' );
      }
}

add_action('admin_head', 'my_add_mce_button');

function my_add_tinymce_plugin( $plugin_array ) {
$plugin_array['my_mce_button'] = get_template_directory_uri() .'/admin/js/tinymce/everandivy_btn.js?ver=1';
$plugin_array['first_media_social_menu_button'] = get_template_directory_uri() .'/admin/js/tinymce/first_media_social_menu_button.js?ver=1';
    return $plugin_array;
}

function my_register_mce_button( $buttons ) {
  array_push( $buttons, 'my_mce_button' );
  array_push( $buttons, 'first_media_social_menu_button' );
  return $buttons;
}


function my_deregister_scripts(){
  wp_dequeue_script( 'wp-embed' );

 }
 add_action( 'wp_footer', 'my_deregister_scripts' );
 remove_action('wp_head', 'print_emoji_detection_script', 7);
 remove_action('wp_print_styles', 'print_emoji_styles');





