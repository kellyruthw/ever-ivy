<?php
// Extend search for document post type
 $post_type = 'document';
 // Custom fields to search for
$custom_fields = array(
	"type",
);

if( ! is_admin() )
	return;

if ( $query->query['post_type'] != $post_type )
return;

$search_term = $query->query_vars['s'];

// Set to empty, otherwise it won't find anything
$query->query_vars['s'] = '';

if ( $search_term != '' ) {
	$meta_query = array( 'relation' => 'OR' );

	foreach( $custom_fields as $custom_field ) {
		array_push( $meta_query, array(
			'key' => $custom_field,
			'value' => $search_term,
			'compare' => 'LIKE'
		));
	}

	$query->set( 'meta_query', $meta_query );
};

add_action( 'pre_get_posts', 'extend_admin_search' );
?>
