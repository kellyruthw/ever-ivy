var obj_image;
var count_editors = 0;
	
function fix_menu() {
	count = 0;
		
	jQuery('#menu-to-edit .menu-item').each(function(index, element) {
		this_item  = jQuery(this);
			
		/*Ανακτησε τον κωδικό του αντικειμενου*/
		this_item_id = jQuery('.menu-item-settings', this_item).attr('id');
		this_item_id = this_item_id.replace('menu-item-settings-', '');
			
		if (this_item.hasClass('menu-item-depth-0')) {
			jQuery('.theme-menu-fields', this_item).removeClass('theme-hidden-field');
			jQuery('.link-to-original', this_item).removeClass('theme-hidden-field');
			jQuery('.field-column-title', this_item).remove();
			jQuery('.field-custom-banners', this_item).remove();
				
			if (jQuery('.field-custom-mega', this_item).length == 0) {
				this_item.removeClass('theme-mega-active');
				jQuery('.item-type', this_item).html(jQuery('.item-type', this_item).attr('data-title'));
					
				jQuery('<p class="field-custom-mega description-wide description"> \
							<label for="edit-menu-item-column-title-' + this_item_id + '"> \
								<input type="checkbox" id="edit-menu-item-mega_menu-' + this_item_id + '" class="widefat code edit-menu-item-mega" name="menu-item-mega_menu[' + this_item_id + ']" value="1"/>Mega menu \
							</label> \
						</p>').insertBefore(jQuery('.menu-item-actions', this_item))
			}
			
			count++
		}
		else if (this_item.hasClass('menu-item-depth-1')) {
			/*Διεγραψε το checkbox του mega menu */
			jQuery('.field-custom-mega', this_item).remove();
			jQuery('.field-column-banners-img', this_item).remove();
			this_item.removeClass('theme-mega-active-banner');
					
			if (this_item.prev().hasClass('theme-mega-active')) {
				this_item.addClass('theme-mega-active');
					
				this_item_title = jQuery('.item-type', this_item).html();
				/*Κρατησε τον τιτλο σε ενα attribute */
				jQuery('.item-type', this_item).attr('data-title', this_item_title);
				/*Αλλαξε τις ρυθμισεις του container */
				jQuery('.item-type', this_item).html('Column');
				jQuery('.theme-menu-fields', this_item).addClass('theme-hidden-field');
				jQuery('.link-to-original', this_item).addClass('theme-hidden-field');
				/*Ανακτησε τον τιτλο απο το item του container*/
				this_item_vl = jQuery('.menu-item-settings p', this_item).eq(0).find('input').val();
				/*Προσθεσε νεες ρυθμισεις*/
				if (jQuery('.field-column-title input', this_item).length == 0) {
					jQuery('<p class="field-column-title description-wide description theme-column-field"> \
								<label for="edit-menu-item-column-title-' + this_item_id + '"> \
									Column title<br> \
									<input type="text" id="edit-menu-item-column-title-' + this_item_id + '" class="widefat edit-menu-item-column-title" name="menu-item-column-title[' + this_item_id + ']" value="' + this_item_vl + '"> \
								</label> \
							</p> \
							<p class="field-custom-banners description-thin description"> \
								<label for="edit-menu-item-banner-title-' + this_item_id + '"> \
									<input type="checkbox" id="edit-menu-item-banner-' + this_item_id + '" class="widefat code edit-menu-item-banners" name="menu-item-banner[' + this_item_id + ']" value="1"/>Has banners \
								</label> \
							</p>').insertBefore(jQuery('.menu-item-actions', this_item))
				}
			}
			else {
				this_item.removeClass('theme-mega-active');
				jQuery('.item-type', this_item).html(jQuery('.item-type', this_item).attr('data-title'));
				
				jQuery('.theme-menu-fields', this_item).removeClass('theme-hidden-field');
				jQuery('.link-to-original', this_item).removeClass('theme-hidden-field');
				this_item.removeClass('theme-mega-active-banner');
				
				jQuery('.field-column-title', this_item).remove();
				jQuery('.field-custom-mega', this_item).remove();
				jQuery('.field-custom-banners', this_item).remove();
				jQuery('.field-column-banners-img', this_item).remove();
			}
		}
		else if (this_item.hasClass('menu-item-depth-2')) {
			if (this_item.prev().hasClass('theme-mega-active')) {
				this_item.addClass('theme-mega-active');
			}
			else {
				this_item.removeClass('theme-mega-active');
				this_item.removeClass('theme-mega-active-banner');
			}
			
			if ((this_item.prev().find('.field-custom-banners input').is(':checked')) || (this_item.prev().hasClass('theme-mega-active-banner'))) {
				jQuery('.theme-menu-fields', this_item).addClass('theme-hidden-field');
				jQuery('.link-to-original', this_item).addClass('theme-hidden-field');
				
				this_item.addClass('theme-mega-active-banner');
			
				if (jQuery('.field-column-banners-img input', this_item).length == 0) {
					jQuery('<div class="banner-each-menu"> \
								<div class="field-column-banners-img description-thin description theme-column-field" style="height: auto"> \
									<div class="rounded-div" style="padding-bottom: 10px"> \
										<label> \
											Image title<br> \
											<input type="text" class="to-fill widefat edit-menu-item-banner-image-title"> \
										</label> \
										<label> \
											Image Link<br> \
											<input type="text" class="to-fill widefat edit-menu-item-banner-image-link"> \
										</label> \
										<label> \
											File<br> \
											<input style="width: 180px" type="text" class="to-fill widefat edit-menu-item-banner-image-source"> \
											<input type="button" class="button button-primary upload-image" value="Upload image"> \
										</label> \
									</div> \
								</div> \
								<div style="clear: both"></div> \
								<div class="field-column-banners-img description-thin description theme-column-field" style="height: auto"> \
									<div class="rounded-div" style="padding-bottom: 10px"> \
										<label> \
											Image title<br> \
											<input type="text" class="to-fill widefat edit-menu-item-banner-image-title"> \
										</label> \
										<label> \
											Image Link<br> \
											<input type="text" class="to-fill widefat edit-menu-item-banner-image-link"> \
										</label> \
										<label> \
											File<br> \
											<input style="width: 180px" type="text" class="to-fill widefat edit-menu-item-banner-image-source"> \
											<input type="button" class="button button-primary upload-image" value="Upload image"> \
										</label> \
									</div> \
								</div> \
								<input style="width: 180px; display: none;" type="text" id="edit-menu-item-banner-image-source-all-' + this_item_id + '" class="to-add widefat edit-menu-item-banner-image-source" name="menu-item-banner-image-source-all[' + this_item_id + ']"> \
							</div>').insertBefore(jQuery('.menu-item-actions', this_item))
				}
			}
			else {
				jQuery('.field-column-banners-img', this_item).remove();	
				
				jQuery('.theme-menu-fields', this_item).removeClass('theme-hidden-field');
				jQuery('.link-to-original', this_item).removeClass('theme-hidden-field');
				this_item.removeClass('theme-mega-active-banner');
			}
			
			jQuery('.item-type', this_item).html(jQuery('.item-type', this_item).attr('data-title'));
			jQuery('.field-column-title', this_item).remove();
			jQuery('.field-custom-mega', this_item).remove();
			jQuery('.field-custom-banners', this_item).remove();
		}
		else {
			this_item.removeClass('theme-mega-active-banner');
			jQuery('.item-type', this_item).html(jQuery('.item-type', this_item).attr('data-title'));
			jQuery('.field-column-title', this_item).remove();
			jQuery('.field-custom-mega', this_item).remove();
			jQuery('.field-custom-banners', this_item).remove();
			jQuery('.field-column-banners-img', this_item).remove();	
			
			jQuery('.theme-menu-fields', this_item).removeClass('theme-hidden-field');
			jQuery('.link-to-original', this_item).removeClass('theme-hidden-field');
		}
	});	
}

jQuery('#menu-to-edit .menu-item').live('mouseup', function(event) {
	console.log('lala');
	if (!jQuery(event.target).is('a')) {
		setTimeout(function() {
			fix_menu()
		}, 600);
	}
});

jQuery('#menu-to-edit .edit-menu-item-mega').live('click', function(event) {
	if (jQuery(this).is(':checked')) {
		jQuery(this).parents('.menu-item').addClass('theme-mega-active');
	}
	else {
		jQuery(this).parents('.menu-item').removeClass('theme-mega-active');
	}
	
	setTimeout(function() {
		fix_menu()
	}, 600);
});