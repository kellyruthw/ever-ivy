(function () {
    tinymce.PluginManager.add('my_mce_button', function (editor, url) {
        editor.addButton('my_mce_button', {
            text: 'Ever & Ivy',
            icon: false,
            type: 'menubutton',
            menu: [{
                text: 'Image With Caption',
                menu: [{
                    text: 'Add',
                    onclick: function () {
                        editor.windowManager.open({
                            title: 'Image With Caption',
                            body: [{
                                type: 'textbox',
                                name: 'img',
                                label: 'Image',
                                value: '',
                                classes: 'my_input_image',
                            }, {
                                type: 'button',
                                name: 'my_upload_button',
                                label: '',
                                text: 'Upload image',
                                classes: 'my_upload_button',
                            }, {
                                type: 'textbox',
                                name: 'multilineName',
                                label: 'Caption',
                                multiline: true,
                                minWidth: 300,
                                minHeight: 100
                            }],
                            onsubmit: function (e) {
                                editor.insertContent('[image_with_caption text="' + e.data.multilineName + '" image="' + e.data.img + '"]');
                            }
                        });
                    }
                }]
            }, {
                text: 'Single Quote',
                menu: [{
                    text: 'Add',
                    onclick: function () {
                        editor.windowManager.open({
                            title: 'Single Quote',
                            body: [{
                                type: 'textbox',
                                name: 'title',
                                label: 'Title',
                                value: '',
                                classes: 'my_input_image',
                            }, {
                                type: 'textbox',
                                name: 'multilineName',
                                label: 'Caption',
                                multiline: true,
                                minWidth: 300,
                                minHeight: 100
                            }],
                            onsubmit: function (e) {
                                editor.insertContent('[single_quote title="' + e.data.title + '" text="' + e.data.multilineName + '"]');
                            }
                        });
                    }
                }]
            }, {
                text: 'Quote with video',
                menu: [{
                    text: 'Add',
                    onclick: function () {
                        editor.windowManager.open({
                            title: 'Quote with video',
                            body: [
                                // {
                                //     type: 'textbox',
                                //     name: 'img',
                                //     label: 'Poster',
                                //     value: '',
                                //     classes: 'my_input_image',
                                // }, {
                                //     type: 'button',
                                //     name: 'my_upload_button',
                                //     label: '',
                                //     text: 'Upload image',
                                //     classes: 'my_upload_button',
                                // }, 
                                {
                                    type: 'textbox',
                                    name: 'pvideo',
                                    label: 'Preview Video ID'
                                }, {
                                    type: 'textbox',
                                    name: 'video',
                                    label: 'Video ID',
                                },
                                // {
                                //     type: 'textbox',
                                //     name: 'title',
                                //     label: 'Title',
                                //     value: '',
                                // }, 
                                {
                                    type: 'textbox',
                                    name: 'multilineName',
                                    label: 'Caption',
                                    multiline: true,
                                    minWidth: 300,
                                    minHeight: 100
                                }
                            ],
                            onsubmit: function (e) {
                                editor.insertContent('[video_quote  pvideo="' + e.data.pvideo + '" video="' + e.data.video + '" text="' + e.data.multilineName + '"]');
                            }
                        });
                    }
                }]
            }, 
            // {
            //     text: 'Single Video',
            //     menu: [{
            //         text: 'Add',
            //         onclick: function () {
            //             editor.windowManager.open({
            //                 title: 'Single Video',
            //                 body: [{
            //                     type: 'textbox',
            //                     name: 'img',
            //                     label: 'Poster',
            //                     value: '',
            //                     classes: 'my_input_image',
            //                 }, {
            //                     type: 'button',
            //                     name: 'my_upload_button',
            //                     label: '',
            //                     text: 'Upload image',
            //                     classes: 'my_upload_button',
            //                 }, {
            //                     type: 'textbox',
            //                     name: 'pvideo',
            //                     label: 'Preview Video ID'
            //                 }, {
            //                     type: 'textbox',
            //                     name: 'video',
            //                     label: 'Video ID',
            //                 }],
            //                 onsubmit: function (e) {
            //                     editor.insertContent('[single_video poster="' + e.data.img + '" pvideo="' + e.data.pvideo + '" video="' + e.data.video + '"]');
            //                 }
            //             });
            //         }
            //     }]
            // }
            // , 
            {
                text: 'Facebook Ad IA 1',
                menu: [{
                    text: 'Add',
                    onclick: function () {
                        editor.insertContent('[facebook_ia_ad_tag adid="1"]');
                    }
                }]
            }, {
                text: 'Facebook Ad IA 2',
                menu: [{
                    text: 'Add',
                    onclick: function () {
                        editor.insertContent('[facebook_ia_ad_tag adid="2"]');
                    }
                }]
            }, {
                text: 'Facebook Ad IA 3',
                menu: [{
                    text: 'Add',
                    onclick: function () {
                        editor.insertContent('[facebook_ia_ad_tag adid="3"]');
                    }
                }]
            }]
        });
    });
})();


jQuery(document).ready(function ($) {
    $(document).on('click', '.mce-my_upload_button', upload_image_tinymce);

    function upload_image_tinymce(e) {
        e.preventDefault();
        var $input_field = $('.mce-my_input_image');
        var custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Add Image',
            button: {
                text: 'Add Image'
            },
            multiple: false
        });
        custom_uploader.on('select', function () {
            var attachment = custom_uploader.state().get('selection').first().toJSON();
            $input_field.val(attachment.url);
        });
        custom_uploader.open();
    }
});