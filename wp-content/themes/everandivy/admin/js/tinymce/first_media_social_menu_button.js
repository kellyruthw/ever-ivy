//add social embeds to menu
(function() {
  let instagramBtn = editor => {
    return {
      text: "Instagram",
      onclick: function() {
        editor.windowManager.open({
          title: "Instagram Embed",
          body: [
            {
              type: "textbox",
              name: "url",
              label: "url",
              value: ""
            },
            {
              type: "listbox",
              name: "hidecaption",
              label: "Hide caption?",
              // onselect: function(e) {

              // },
              values: [
                { text: "false", value: false },
                { text: "true", value: true }
              ]
            }
          ],
          onsubmit: function(e) {
            if (e.data.url) {
              let content = `[fm_instagram url="${e.data.url}"`;
              if (e.data.hidecaption || e.data.hidecaption === "true") {
                content += " hidecaption=true";
              }
              content += "]";
              editor.insertContent(content);
            }
          }
        });
      }
    };
  };

  let pinterestBtn = editor => {
    return {
      text: "Pinterest",
      onclick: function() {
        editor.windowManager.open({
          title: "Pinterest Embed",
          body: [
            {
              type: "textbox",
              name: "url",
              label: "url",
              value: ""
            },
            {
              type: "listbox",
              name: "hidecaption",
              label: "Hide caption?",
              // onselect: function(e) {

              // },
              values: [
                { text: "false", value: false },
                { text: "true", value: true }
              ]
            }
          ],
          onsubmit: function(e) {
            if (e.data.url) {
              let content = `[fm_pinterest url="${e.data.url}"`;
              if (e.data.hidecaption || e.data.hidecaption === "true") {
                content += " hidecaption=true";
              }
              content += "]";
              editor.insertContent(content);
            }
          }
        });
      }
    };
  };

  let giphyBtn = editor => {
    return {
      text: "Giphy",
      onclick: function() {
        editor.windowManager.open({
          title: "Giphy Embed",
          body: [
            {
              type: "textbox",
              name: "url",
              label: "url",
              value: ""
            }
          ],
          onsubmit: function(e) {
            if (e.data.url) {
              let content = `[fm_giphy url="${e.data.url}"`;
              content += "]";
              editor.insertContent(content);
            }
          }
        });
      }
    };
  };

  let facebookBtn = editor => {
    return {
      text: "Facbook",
      onclick: function() {
        editor.windowManager.open({
          title: "Facbook Embed",
          body: [
            {
              type: "textbox",
              name: "url",
              label: "url",
              value: ""
            }
          ],
          onsubmit: function(e) {
            if (e.data.url) {
              let content = `[fm_facebook url="${e.data.url}"`;
              content += "]";
              editor.insertContent(content);
            }
          }
        });
      }
    };
  };

  let twitterBtn = editor => {
    return {
      text: "Twitter",
      onclick: function() {
        editor.windowManager.open({
          title: "Twitter Embed",
          body: [
            {
              type: "textbox",
              name: "url",
              label: "url",
              value: ""
            }
          ],
          onsubmit: function(e) {
            if (e.data.url) {
              let content = `[fm_twitter url="${e.data.url}"`;
              content += "]";
              editor.insertContent(content);
            }
          }
        });
      }
    };
  };

  let youtubeBtn = editor => {
    return {
      text: "Youtube",
      onclick: function() {
        editor.windowManager.open({
          title: "Youtube Embed",
          body: [
            {
              type: "textbox",
              name: "url",
              label: "url",
              value: ""
            }
          ],
          onsubmit: function(e) {
            if (e.data.url) {
              let content = `[fm_youtube url="${e.data.url}"`;
              content += "]";
              editor.insertContent(content);
            }
          }
        });
      }
    };
  };

  let imgurBtn = editor => {
    return {
      text: "Imgur",
      onclick: function() {
        editor.windowManager.open({
          title: "Imgur Embed",
          body: [
            {
              type: "textbox",
              name: "url",
              label: "url",
              value: ""
            },
            {
              type: "listbox",
              name: "hidecaption",
              label: "Hide caption?",
              // onselect: function(e) {

              // },
              values: [
                { text: "false", value: false },
                { text: "true", value: true }
              ]
            }
          ],
          onsubmit: function(e) {
            if (e.data.url) {
              let content = `[fm_imgur url="${e.data.url}"`;
              if (e.data.hidecaption || e.data.hidecaption === "true") {
                content += " hidecaption=true";
              }
              content += "]";
              editor.insertContent(content);
            }
          }
        });
      }
    };
  };

  tinymce.PluginManager.add("first_media_social_menu_button", function(
    editor,
    url
  ) {
    editor.addButton("first_media_social_menu_button", {
      text: "Social Embeds",
      icon: false,
      type: "menubutton",
      menu: [instagramBtn(editor), pinterestBtn(editor), giphyBtn(editor), facebookBtn(editor), twitterBtn(editor), youtubeBtn(editor)]
    });
  });
})();
