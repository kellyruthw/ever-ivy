import $ from "jquery";
function loadInstagram(el) {


    let isLoadedAlready = el.getAttribute('ever-ivy');
    // if (isLoadedAlready) {
    //   return;
    // }
    
    $.get("https://api.instagram.com/oembed?url=http://instagr.am/p/" + el.id, function (data) {
      
      $("#" + el.id).append(data.html);
    });
    el.setAttribute('ever-ivy', true);
  
  
  };
  
  function loadTwitter(el) {
  
  
    let isLoadedAlready = el.getAttribute('ever-ivy');
    // if (isLoadedAlready) {
    //   return;
    // }
    var sizeWidth = (screen.width > 780) ? 500 : 350;
  
            $.get("/twitter-serve.php/?id=" +el.id+ "&width=" + sizeWidth ,function(data) { 
                  $("#" +el.id) .append(data.html);
            }); 
    el.setAttribute('ever-ivy', true);
  
  
  }
  
  
  function loadEmbeds(){
    
    $('.bu-social-embed-instagram').each((index , el)=>{
      loadInstagram(el);
    });
    
    $('.bu-social-embed-twitter').each((index , el)=>{
      loadTwitter(el);
    });
  
  }

  export {loadEmbeds};