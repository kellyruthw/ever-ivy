import $ from 'jquery';
const dfpAccountId = '2699062';
const adsSettings = {
    "ENI_Article_D300x600_3": {
        size: ['fluid', [1, 1],
            [300, 250],
            [300, 600]
        ]
    },
    "ENI_Article_D300x600_4": {
        size: ['fluid', [1, 1],
            [300, 250],
            [300, 600]
        ]
    },
    "ENI_Article_D300x250_A_inContent":{
        size: ['fluid', [1, 1],
            [300, 250],
            [728, 90]
        ]

    },
    "ENI_Article_M300x250_A_inContent":{
        size: ['fluid',[1, 1] ,  [320, 50],
            [300, 250],
            [320, 100]
        ]

    }

};
const adWrapperClassName = "ad-gpt";


function getAdUnitNameFromAdUnitCode(window, wrapperId) {
    let adUnitName = wrapperId.replace(/gpt_\d_/, "");
    return adUnitName;

};

function defineSlot(window, adUnitCode) {

    let adUnitName = getAdUnitNameFromAdUnitCode(window, adUnitCode);
    let slot = window.googletag.defineSlot(`/${dfpAccountId}/${adUnitName}`, adsSettings[adUnitName].size, adUnitCode).addService(window.googletag.pubads());
    return slot;

};


function injectInContent(){
    if ($(window).width() > 600) {
        [...document.querySelectorAll( ".single .entry-content h3" )].forEach((elem , index)=>{
            $(elem).before( "<div id='gpt_" + index + "_ENI_Article_D300x250_A_inContent' class='ad-gpt'></div>" );
        });
     }
     else {
        [...document.querySelectorAll( ".single .entry-content h3" )].forEach((elem , index)=>{
            $(elem).before( "<div id='gpt_" + index + "_ENI_Article_M300x250_A_inContent' class='ad-gpt'></div>" );
        });
     }
}



function initDfp(window) {

    injectInContent();
    window.googletag.cmd.push(function () {
        let adWrapperElemsArr = [...document.querySelectorAll(`.${adWrapperClassName}`)];
        let slots = [];
        adWrapperElemsArr.forEach(adWrapper=>{
            let adUnitCode = adWrapper.id;
            let slot = defineSlot(window , adUnitCode);
            slots.push(slot);
        });

        googletag.enableServices();
        slots.forEach(slot=>{
            googletag.display(slot);
        });


    });

}


export default (window) => {
    window.googletag = window.googletag || {
        cmd: []
    };
    initDfp(window);


};