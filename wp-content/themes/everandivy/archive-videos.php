<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bestsubscriptions
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="content">
				<?php $this_category = get_category($cat); ?>
				
				<div class="container">
					<h1>Videos</h1>
					<div class="articles">
					
							<?php
							// set the "paged" parameter (use 'page' if the query is on a static front page)
							$page = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;
							
							?>
							
							
							<?php
							// the loop
							while ( have_posts() ) : the_post(); 
													
							?>
							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							    <a href="<?php echo esc_url( get_permalink() ); ?>">
								    <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-play.svg" class="play-btn">
								    <?php the_post_thumbnail('homepage-recent-thumb'); ?>
								</a>
							    <div class="copy">
							        <h2><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title(); ?></a></h2>
							    </div>
						    </article>
							<?php endwhile; ?>
							<div class="navigation">						
								<div class="next">
									<?php next_posts_link( 'Older Entries', $the_query2->max_num_pages ); ?>
								</div>
							</div>
							<?php 
							// clean up after the query and pagination
							wp_reset_postdata(); 
							?>
							
							<!-- /2699062/ei_category_d970x250_1 -->
							<div id='div-gpt-ad-1558571820919-0' class="ad">
								<script>
								googletag.cmd.push(function() { googletag.display('div-gpt-ad-1558571820919-0'); });
								</script>
							</div>
							
						</div>
						<?php
					if ( wp_is_mobile() ) { ?>
						<div class="view-more mobile">
							<div class="page-load-status">
							  <div class="loader-ellips infinite-scroll-request">
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							  </div>
							</div>
							<button class="btn">Show me more</button>
						</div>
						
						<?php get_sidebar(); ?>

					<?php } else { ?>
						<?php get_sidebar(); ?>
						<div class="view-more">
							<div class="page-load-status">
							  <div class="loader-ellips infinite-scroll-request">
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							    <span class="loader-ellips__dot"></span>
							  </div>
							</div>
							<button class="btn">Show me more</button>
						</div>
					<?php }
				?>
				</div>
			</div> <!-- content -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>

				