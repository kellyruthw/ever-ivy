<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bestsubscriptions
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php the_category(); ?>
	<h1><?php the_title(); ?></h1>
		<div class="entry-meta">
			<div class="date">
			By: <span><?php echo get_the_author(); ?></span><?php echo get_the_date(); ?><!--<span><?php the_time( $d ); ?></span>-->
			</div>
		</div><!-- .entry-meta -->
	<?php the_post_thumbnail('single-thumbnail'); ?>
	<div class="contain">
		<div class="social-share">
			<h3>Share</h3>
			<?php
			    global $post;
			    $post_slug = $post->post_name;			    
			?>
			<a href="http://www.facebook.com/sharer/sharer.php?u=http%3A//everandivy.com/<?php echo $post_slug; ?>" data-network="facebook"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ss-fb.svg"></a>
			<a href="http://twitter.com/home?status=http%3A//everivyv2.wpengine.com/<?php echo $post_slug; ?>/" data-network="twitter"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ss-tw.svg"></a>
			<a href="http://pinterest.com/pin/create/button/?url=http%3A//everivyv2.wpengine.com/<?php echo $post_slug; ?>/&media=&description=" data-network="pinterest"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ss-pin.svg"></a>
			<a href="mailto:?body=I%20think%20you'd%20like%20this%20article,%20http%3A//everivyv2.wpengine.com/<?php echo $post_slug; ?>/"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ss-email.svg"></a>
		</div>
		<div class="entry-content">
			<?php the_content(); ?>
		</div><!-- .entry-content -->
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
