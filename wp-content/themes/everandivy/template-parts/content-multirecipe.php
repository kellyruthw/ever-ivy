<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bestsubscriptions
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<!--<div class="post-categories">
	<?php
		$terms = get_terms( 'recipe_cat' );
 
		echo '<ul>';
		foreach ( $terms as $term ) {
		    $term_link = get_term_link( $term );
		    if ( is_wp_error( $term_link ) ) {
		        continue;
		    }
		    echo '<li><a href="' . esc_url( $term_link ) . '">' . $term->name . '</a></li>';
		}
		 
		echo '</ul>';
		?>
	</div>
	-->
	<h1><?php the_title(); ?></h1>
	<div class="recipe-video">
		<?php if( have_rows('video') ): ?>
			<?php while( have_rows('video') ): the_row(); ?> 
				<?php 
					$videoid = get_sub_field('media_id'); 	
					$url = 'https://cdn.jwplayer.com/players/' .$videoid. '-Igg1JvZW.html';
				?>

				<div style="position:relative; padding-bottom:100%; overflow:hidden;">
					<iframe src="<?php echo $url; ?>" width="100%" height="100%" frameborder="0" scrolling="auto" allowfullscreen style="position:absolute;"></iframe>
				</div>
				
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
	<div class="copy">
		<ul class="details">
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-prep-time.svg">
				<div class="label">Prep Time<span>30min</span></div>
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-total-time.svg">
				<div class="label">Total Time<span>30min</span></div>
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-complexity.svg">
				<div class="label">Complexity<span>30min</span></div>
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-serves.svg">
				<div class="label">Serves<span>30min</span></div>
			</li>
		</ul>
		<?php the_content(); ?>
		<div class="social-share">
			<h3>Share</h3>
			<div class="contain">
				<a href="https://www.facebook.com/sharer/sharer.php?u=https%3A//soyummy2.wpengine.com/mcdonalds-breakfast-change/"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ss-fb.svg"></a>
				<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ss-tw.svg"></a>
				<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ss-pin.svg"></a>
				<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ss-email.svg"></a>
			</div>
		</div>
	</div>
	<?php 
	$posts = get_field('recipes_in_this_video');
	
	if( $posts ): ?>
	<div class="in-this-video">
		<h2>In this Video</h2>
	   
	    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
	    	<?php
		    	global $post;
				$postcat = get_the_category( $post->ID );
				$do_not_duplicate[] = $post->ID; 
				?>
	        <?php setup_postdata($post); ?>
	        <article>
	            <a href="<?php the_permalink(); ?>">
		            <?php the_post_thumbnail(); ?>
		            <h3><?php the_title(); ?></h3>
		        </a>
	        </article>
	    <?php endforeach; ?>
	 
	    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	    </div>
	<?php endif; ?>
	
	
	
	<?php
	if( have_rows('ingredients') ):
	?>
	<div class="steps-ingredients">
		<div class="ingredients">
			<h2>Ingredients</h2>
			<ul>
			<?php
		    while ( have_rows('ingredients') ) : the_row(); ?>
		    
				<li>
		        	<div class="amount"><?php the_sub_field('amount'); ?></div>
		        	<p><?php the_sub_field('text'); ?></p>
		        </li>
		
				<?php endwhile; ?>
			</ul>
		</div>
		<?php endif; ?>
		<?php
		
		if( have_rows('steps') ):
		?>
		
		<div class="directions">
			<h2>Directions</h2>
			<ul>
			<?php
		    while ( have_rows('steps') ) : the_row(); ?>
		    
				<li>
		        	<h4><?php the_sub_field('title'); ?></h4>
					<?php the_sub_field('text'); ?>
		        </li>
		
				<?php endwhile; ?>
			</ul>
		</div>
	</div>
	<?php endif; ?>
		
	
</article><!-- #post-<?php the_ID(); ?> -->