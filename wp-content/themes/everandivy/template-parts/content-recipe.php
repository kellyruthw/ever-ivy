<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bestsubscriptions
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<!--<div class="post-categories">
	<?php
		$terms = get_terms( 'recipe_cat' );
 
		echo '<ul>';
		foreach ( $terms as $term ) {
		    $term_link = get_term_link( $term );
		    if ( is_wp_error( $term_link ) ) {
		        continue;
		    }
		    echo '<li><a href="' . esc_url( $term_link ) . '">' . $term->name . '</a></li>';
		}
		 
		echo '</ul>';
		?>
	</div>
	-->
	<h1><?php the_title(); ?></h1>
	<div class="recipe-video">
		<?php if( have_rows('video') ): ?>
			<?php while( have_rows('video') ): the_row(); ?> 
				<?php 
					$videoid = get_sub_field('media_id'); 	
					$url = 'https://cdn.jwplayer.com/players/' .$videoid. '-Igg1JvZW.html';
				?>

				<div style="position:relative; padding-bottom:100%; overflow:hidden;">
					<iframe src="<?php echo $url; ?>" width="100%" height="100%" frameborder="0" scrolling="auto" allowfullscreen style="position:absolute;"></iframe>
				</div>
				
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
	<div class="copy">
		<?php if (get_field("prep_time") ) { ?>
		<ul class="details">
			<?php if (get_field("prep_time") ) { ?>
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-prep-time.svg">
				<div class="label">Prep Time<span><?php the_field("prep_time"); ?></span></div>
			</li>
			<?php } ?>
			<?php if (get_field("total_time") ) { ?>
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-total-time.svg">
				<div class="label">Total Time<span><?php the_field("total_time"); ?></span></div>
			</li>
			<?php } ?>
			<?php if (get_field("complexity") ) { ?>
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-complexity.svg">
				<div class="label">Complexity<span><?php the_field("complexity"); ?></span></div>
			</li>
			<?php } ?>
			<?php if (get_field("servings") ) { ?>
			<li>
				<img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-serves.svg">
				<div class="label">Serves<span><?php the_field("servings"); ?></span></div>
			</li>
			<?php } ?>
		</ul>
		<?php } ?>
		
		<?php
		if( have_rows('video') ):
		?>
			<?php
		    while ( have_rows('video') ) : the_row(); ?>
	    
	        	<?php the_sub_field('video_description'); ?>
	
			<?php endwhile; ?>
			<?php if ( get_field( 'is_sponsored' ) ): ?>
		<h4 class="sponsored">Sponsored by <a href="<?php the_field("sponsored_link"); ?>"><?php the_field("sponsor_name"); ?></a></h4>
		<?php endif; ?>
		<?php endif; ?>
		<div class="social-share">
			<h3>Share</h3>
			<div class="contain">
				<a href="https://www.facebook.com/sharer/sharer.php?u=https%3A//soyummy2.wpengine.com/mcdonalds-breakfast-change/"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ss-fb.svg"></a>
				<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ss-tw.svg"></a>
				<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ss-pin.svg"></a>
				<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ss-email.svg"></a>
			</div>
		</div>
	</div>
	<?php 
	$posts = get_field('recipes_in_this_video');
	
	if( $posts ): ?>
	<div class="in-this-video">
		<h2>In this Video</h2>
	   
	    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
	    	<?php
		    	global $post;
				$postcat = get_the_category( $post->ID );
				$do_not_duplicate[] = $post->ID; 
				?>
	        <?php setup_postdata($post); ?>
	        <article>
	            <a href="<?php the_permalink(); ?>">
		            <?php the_post_thumbnail(); ?>
		            <h3><?php the_title(); ?></h3>
		        </a>
	        </article>
	    <?php endforeach; ?>
	 
	    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	    </div>
	<?php endif; ?>
	
	<?php 

	// check for rows (parent repeater)
	if( have_rows('ingredients') ): ?>
		<div class="steps-ingredients">
			<div class="ingredients">
			
			<?php 
	
			// loop through rows (parent repeater)
			while( have_rows('ingredients') ): the_row(); ?>
				<?php if (get_sub_field('title')) { ?>
				
					<h2><?php the_sub_field('title'); ?></h2>
				
				<?php } else { ?>
					
				  <h2>Ingredients</h2>
				  
				<?php } ?>
				
				<?php 

				// check for rows (sub repeater)
				if( have_rows('ingredients_type') ): ?>
					<ul>
					<?php 

					// loop through rows (sub repeater)
					while( have_rows('ingredients_type') ): the_row();

						// display each item as a list - with a class of completed ( if completed )
						?>
						<li>
			        		<div class="amount"><?php the_sub_field('amount'); ?></div>
				        	<p><?php the_sub_field('text'); ?></p>
				        </li>
					<?php endwhile; ?>
					</ul>
				<?php endif; //if( get_sub_field('items') ): ?>
	
			<?php endwhile; // while( has_sub_field('to-do_lists') ): ?>
			</div>
			<div class="directions">
				<h2>Directions</h2>
				<?php the_content(); ?>
			</div>
		</div>
	<?php endif; // if( get_field('to-do_lists') ): ?>

</article><!-- #post-<?php the_ID(); ?> -->