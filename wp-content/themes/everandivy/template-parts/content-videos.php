<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package bestsubscriptions
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="post-categories">
	<?php
		$terms = get_terms( 'video_cat' );
 
		echo '<ul>';
		foreach ( $terms as $term ) {
		    $term_link = get_term_link( $term );
		    if ( is_wp_error( $term_link ) ) {
		        continue;
		    }
		    echo '<li><a href="' . esc_url( $term_link ) . '">' . $term->name . '</a></li>';
		}
		 
		echo '</ul>';
		?>
	</div>

	<h1><?php the_title(); ?></h1>
	<div class="entry-meta">
		<div class="date">
			<?php echo get_the_date(); ?><!--<span><?php the_time( $d ); ?></span>-->
		</div>
	</div><!-- .entry-meta -->
	<?php 
		$url = get_field("youtube_url");
		$goodUrl = str_replace('/watch?v=', '/embed/', $url);
	?>
	
	<div class="videoWrapper">
		<iframe width="560" height="349" src="<?php echo $goodUrl; ?>" frameborder="0" allowfullscreen></iframe>
	</div>
	
	<div class="contain">
		<div class="social-share">
			<h3>Share</h3>
			<a href="https://www.facebook.com/sharer/sharer.php?u=https%3A//soyummy2.wpengine.com/mcdonalds-breakfast-change/"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ss-fb.svg"></a>
			<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ss-tw.svg"></a>
			<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ss-pin.svg"></a>
			<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-ss-email.svg"></a>
		</div>
		<div class="entry-content">
			<?php the_content(); ?>
		</div><!-- .entry-content -->
	</div>

</article><!-- #post-<?php the_ID(); ?> -->
<?php next_post_link( '%link', 'Next post in category', TRUE ); ?>
